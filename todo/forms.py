from django import forms
from .models import Todo

class TodoForm(forms.Form):
    text = forms.CharField(max_length=40, widget=forms.TextInput(
        attrs={
            'class': 'form-control col-9',
            'placeholder': 'Enter something'
        }))


class NewTodoForm(forms.ModelForm):
    class Meta:
        model = Todo
        fields = ('text', )

        widgets = {
            'text': forms.TextInput(attrs={
                'class': 'form-controle col-9',
                'placeholder': 'Enter something...'
            })
        }
