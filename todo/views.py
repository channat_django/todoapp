from django.shortcuts import render, redirect
from .models import Todo
from .forms import TodoForm, NewTodoForm
from django.views.decorators.http import require_POST

def index(request):
    todos = Todo.objects.order_by('-id')
    form = TodoForm()
    newForm = NewTodoForm()
    context = {
        'todo_list': todos,
        'form': newForm
    }
    return render(request, 'todo/index.html', context)


@require_POST
def add_todo(request):
    #form = TodoForm(request.POST)
    newForm = NewTodoForm(request.POST)

    if  newForm.is_valid:
        new_todo = newForm.save()
        # new_todo = Todo(text=request.POST['text'])
        # new_todo.save()

    return redirect('index')


def add_to_complete(request, todo_id):
    todo = Todo.objects.get(pk=todo_id)
    todo.complete = True
    todo.save()

    return redirect('index')


def delete_completed(request):
    Todo.objects.filter(complete=True).delete()
    return redirect('index')

def delete_all_todo(request):
    Todo.objects.all().delete()
    return redirect('index')
