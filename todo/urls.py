from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add_to_complete/<todo_id>/', views.add_to_complete, name='add_to_complete'),
    path('add_todo/', views.add_todo, name='add_todo'),
    path('delete_completed/', views.delete_completed, name='delete_completed'),
    path('delete_all_todo/', views.delete_all_todo, name='delete_all_todo')

]
